#!/bin/sh

FILE_PATH=`env readlink -f $0`
BIN_DIR=`env dirname ${FILE_PATH}`

source $BIN_DIR/setenv.d/properties.sh
source $BIN_DIR/setenv.d/opts.sh
FILES=`find $BIN_DIR/setenv.d/catalina_opts.d/*.sh -maxdepth 1 -type f -exec echo {} \;`
for FILE in $FILES
do
  echo "Config '$FILE' is loaded"
  source $FILE
done;

CATALINA_OPT="-server -Dspring.profiles.active=integration -Dasset.timestamp.generator=PROPERTY_FILE {{ '-Dconfig.override=' + cmdb_var.before_integration_override if cmdb_var.before_integration_override else '' }}"
CATALINA_OPT="$CATALINA_OPT $JVM_MEMORY ${!GC_TYPE} $GC_LOGGING $JMX"
CATALINA_OPTS="$CATALINA_OPT $CATALINA_OPTS"
JAVA_OPTS="$JAVA_OPTS $SERVER_XML_PROPS"
